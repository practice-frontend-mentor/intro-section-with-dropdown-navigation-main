/*Obtención de elementos*/
let drpdwn=document.querySelectorAll('.menu-dropdown');

/*Menu dropdown*/
drpdwn.forEach((dd)=>{
    dd.addEventListener('click',()=>{
        let el =dd.lastElementChild;

        for(i=0;i<drpdwn.length;i++){
            if(drpdwn[i].lastElementChild==el){
                el.classList.contains('show')?el.classList.remove('show'):el.classList.add('show');
                el.classList.contains('show')?dd.firstChild.nextElementSibling.firstElementChild.firstElementChild.style.rotate='180deg':dd.firstChild.nextElementSibling.firstElementChild.firstElementChild.style.rotate='none';
            }else if(drpdwn[i].lastElementChild.classList.contains('show')){
                drpdwn[i].lastElementChild.classList.remove('show');
                drpdwn[i].firstChild.nextElementSibling.firstElementChild.firstElementChild.style.rotate='none';
            }
        }
    })
});